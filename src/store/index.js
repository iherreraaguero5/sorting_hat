import Vue from "vue";
import Vuex from "vuex";
import hat from "@/store/modules/hat";
Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    hat,
  },
});
