const SET_NAME = "SET_NAME";
const POP_QUESTION = "POP_QUESTION"; // to give the option to roll back an answer
const PUSH_QUESTION = "PUSH_QUESTION";
const CLEAR_DATA = "CLEAR_DATA";
const state = {
  question_index: 0,
  name: "",
  question_stack: [],
};
const getters = {
  getSchool: (state) => {
    let totals = { g: 0, r: 0, h: 0, s: 0 };
    state.question_stack.forEach((item) => {
      let scores = item.scores;
      totals.g += scores.g;
      totals.r += scores.r;
      totals.h += scores.h;
      totals.s += scores.s;
    });
    let sum = totals.g;
    let school = "Gryffindor";
    if (sum < totals.r) {
      school = "Ravenclaw";
      sum = totals.r;
    }
    if (sum < totals.h) {
      school = "Hufflepuff";
      sum = totals.h;
    }
    if (sum < totals.s) return "Slytherin";
    return school;
  },
};
const mutations = {
  SET_NAME(state, name) {
    state.name = name;
  },
  POP_QUESTION(state) {
    state.question_stack.pop();
    state.question_index--;
  },
  PUSH_QUESTION(state, question) {
    state.question_stack.push(question);
    state.question_index++;
  },
  CLEAR_DATA(state) {
    state.name = "";
    state.question_stack = [];
    state.question_index = 0;
  },
};
const actions = {
  setName({ commit }, name) {
    commit(SET_NAME, name);
  },
  pushQuestion({ commit }, question) {
    commit(PUSH_QUESTION, question);
  },
  popQuestion({ commit }, question) {
    commit(POP_QUESTION, question);
  },
  reset({ commit }) {
    commit(CLEAR_DATA);
  },
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};
